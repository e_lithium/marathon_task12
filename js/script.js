/* Задания на урок:

1) Реализовать функционал, что после заполнения формы и нажатия кнопки "Подтвердить" - 
новый фильм добавляется в список. Страница не должна перезагружаться.
Новый фильм должен добавляться в movieDB.movies.
Для получения доступа к значению input - обращаемся к нему как input.value;
P.S. Здесь есть несколько вариантов решения задачи, принимается любой, но рабочий.

2) Если название фильма больше, чем 21 символ - обрезать его и добавить три точки

3) При клике на мусорную корзину - элемент будет удаляться из списка (сложно)

4) Если в форме стоит галочка "Сделать любимым" - в консоль вывести сообщение: 
"Добавляем любимый фильм"

5) Фильмы должны быть отсортированы по алфавиту */


'use strict';

// убирается реклама и меняется фон из предыдущего задания
const addblock = document.querySelectorAll(".promo__adv img");

addblock.forEach(item => {
    item.remove();
})

const a = document.querySelector(".promo__genre");
a.textContent = "ДРАМА";

const posterBg = document.querySelector(".promo__bg");
posterBg.style.background = 'url("img/bg.jpg")';

//----------------------

const movieDB = {
    movies: [
        "Логан",
        "Лига справедливости",
        "Ла-ла лэнд",
        "Одержимость",
        "Скотт Пилигрим против..."
    ]
};

//список кнопок для удаления фильмов
let deleteButtons;

//обновление списка фильмов из movieDB
function updateMovieList() {
    const movieList = document.querySelector(".promo__interactive-list");

    movieList.innerHTML = "";

    movieDB.movies.sort();

    movieDB.movies.forEach((item, i) => {
        movieList.innerHTML = movieList.innerHTML + `
         <li class="promo__interactive-item"> ${i + 1}. ${item}
             <div class="delete"></div>
         </li>`;
    });

    //после изменения в списке привязать/отвязать обработчик событий для добавленных/удаленных элементов
    updateDeleteButtons();
}

//добавить всем кнопкам для удаления обработчик события нажатия
function updateDeleteButtons() {
    deleteButtons = document.querySelectorAll('.delete');
    deleteButtons.forEach((button, id) => {
        button.addEventListener('click', (event) => deleteMovieFromList(event, id));
    });
}


//удаление фильма из списка
function deleteMovieFromList(evt, id){
    evt.currentTarget.parentElement.remove();
    movieDB.movies.splice(id, 1);
    updateMovieList();
};


const submitButton = document.querySelector(".add button");

//добавить фильм в список
submitButton.addEventListener('click', function (event) {
    event.preventDefault();

    let newMovie = document.querySelector('.adding__input').value;

    if (newMovie.length > 21) {
        newMovie = newMovie.slice(0, 21) + '...';
    };

    movieDB.movies.push(newMovie);

    updateMovieList();

    let favoriteCheckboxStatus = document.querySelector('.add input[type="checkbox"]');

    if (favoriteCheckboxStatus.checked) {
        console.log('Добавляем любимый фильм');
    };

});

updateMovieList();